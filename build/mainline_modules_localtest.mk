#
# Copyright 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Mainline modules - APK type for local testing
PRODUCT_PACKAGES += \
    CaptivePortalLoginLocalTest \
    NetworkStackLocalTest \

ifneq ($(filter $(PLATFORM_SDK_VERSION),29 30 31 32),)
    PRODUCT_PACKAGES += NetworkPermissionConfigLocalTest \

endif

# Ingesting networkstack.x509.pem for local testing
PRODUCT_MAINLINE_SEPOLICY_DEV_CERTIFICATES:=vendor/partner_modules/LocalTestPrebuilts/certificates

# Mainline modules - APEX type for local testing
PRODUCT_PACKAGES += \
    com.android.resolv.localtest \
    com.android.tethering.localtest \
    com.android.cellbroadcast.localtest \

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.android.resolv.localtest.apex \
    system/apex/com.android.tethering.localtest.apex \
    system/apex/com.android.cellbroadcast.localtest.apex \
    system/app/CaptivePortalLoginLocalTest.apk \
    system/priv-app/NetworkStackLocalTest.apk \
    system/priv-app/NetworkPermissionConfigLocalTest.apk \

